# 搭建手册下载
## 第一期
<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><img src="images/3D-light.png" width="150px;"></td>
<td width="25%;"><img src="images/color-TV.png" width="150px;"></td>
<td width="25%;"><img src="images/dynamic-light.png" width="150px;"></td>
<td width="25%;"><img src="images/tree.png" width="150px;"></td></tr>
<tr>
<td width="25%;"><a href="manual/3D立体灯.rar" download style="font-weight:bold;">3D立体灯</a></td>
<td width="25%;"><a href="manual/变色彩电.rar" download style="font-weight:bold;">变色彩电</a></td>
<td width="25%;"><a href="manual/动感光影灯.rar" download style="font-weight:bold;">动感光影灯</a></td>
<td width="25%;"><a href="manual/光树.rar" download style="font-weight:bold;">光树</a></td></tr>
</table>

## 第二期
<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><img src="images/music-box.png" width="150px;"></td>
<td width="25%;"><img src="images/plane.png" width="150px;"></td>
<td width="25%;"><img src="images/bracelet.png" width="150px;"></td>
<tr>
<td width="25%;"><a href="manual/音乐盒.rar" download style="font-weight:bold;">音乐盒</a></td>
<td width="25%;"><a href="manual/小飞机.rar" download style="font-weight:bold;">小飞机</a></td>
<td width="25%;"><a href="manual/魔术手环.rar" download style="font-weight:bold;">魔术手环</a></td></tr>
</table>

## 第三期
<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><img src="images/turntable.png" width="150px;"></td>
<td width="25%;"><img src="images/cat.png" width="150px;"></td>
<td width="25%;"><img src="images/monster.png" width="150px;"></td>
<tr>
<td width="25%;"><a href="manual/游戏转盘.rar" download style="font-weight:bold;">游戏转盘</a></td>
<td width="25%;"><a href="manual/调皮的小动物.rar" download style="font-weight:bold;">调皮的小动物</a></td>
<td width="25%;"><a href="manual/大嘴怪.rar" download style="font-weight:bold;">大嘴怪</a></td></tr>
</table>