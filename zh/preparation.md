# 「童心制物编程造物盒」软件准备帮助文档

## 软件准备

在上课前，除了进入班级和添加班主任微信外，您还需要在开课前下载好以下课堂必备软件：

<div style="background-color:#B0E0E6;font-weight:bold;padding:12px 0px 5px 6px;border-radius:6px;margin-bottom:12px;line-height:36px;width:90%;">
<h4>&#10065; 如果您希望使用电脑学习</h4>
请访问 <a href='https://hezi.makeblock.com/#/' target='_blank'>hezi.makeblock.com</a>，按照网页提示：
<ul >
<li>第一步，安装 Chrome 浏览器 <a href="#1" style="font-size:14px;">&rarr; [Chrome 浏览器安装]</a></li>
<li>第二步，下载、安装并启动mLink 插件 <a href="#mlink安装" style="font-size:14px;">&rarr; [mLink 插件安装]</a></li>
<li>第三步，使用Chrome浏览器访问 <a href='https://hezi.makeblock.com/#/' target='_blank'>hezi.makeblock.com</a>，开启学习</li>
</ul>
<h4>&#10065; 如果您希望使用 iPad 学习</h4>
登录App Store，搜索并下载慧编程APP
</div>

<small>注：目前仅支持电脑网页端和iPad端学习。</small>

*如果您在电脑安装软件过程中遇到问题，您可以阅读以下问题来获取问题答案。*


### 第一步：安装 Chrome 浏览器

请点击以下网址 [https://www.google.cn/chrome/](https://www.google.cn/chrome/) 按照提示安装Chrome浏览器。

### 第二步：下载、安装和启动 mLink 插件{#mlink安装}

#### 1\. 安装前请确认电脑系统是否支持

- Windows 7及以上（建议使用64位系统）
- MacOS 10.10及以上

&nbsp; &nbsp;<small><a href="#3">如何获知电脑是什么系统？</a></small><br>
&nbsp; &nbsp;<small><a href="#5">如何获知电脑系统版本？</a></small>

#### 2\. 下载并快速安装 mLink

##### _Windows用户安装步骤_

1\. 下载mLink

[mLink-Windows安装包](http://mblockapp.oss-cn-hongkong.aliyuncs.com/mblock5/win32/mLinkSetup.exe)

2\. 请按照弹窗的提示，正确安装 mLink

<img src="images/install-1.png" width="400px;" style="padding:3px 0px 12px 0px;">

<img src="images/install-2.png" width="300px;" style="padding:3px 0px 12px 0px;">

<img src="images/install-3.png" width="400px;" style="padding:3px 0px 12px 0px;">

<img src="images/install-4.png" width="400px;" style="padding:3px 0px 12px 0px;">

<img src="images/install-5.png" width="400px;" style="padding:3px 0px 12px 0px;">

<img src="images/install-6.png" width="400px;" style="padding:3px 0px 12px 0px;">

3\. 安装完毕后，双击电脑桌面上 mLink 图标启动

<img src="images/install-7.png"  style="padding:3px 0px 12px 0px;">

成功启动 mLink 的标志是，mLink 会出现在桌面右下角的任务栏里（如果不知道任务栏的位置，可参考[常见问题](#6)）。

<small>注：每次学习前，都需要启动mLink。</small>

<img src="images/install-8.png" width="400px;" style="padding:3px 0px 12px 0px;">

4\. 成功啦，进入第三步的操作

##### _MacOS用户安装步骤_

1\. 下载mLink

[mLink-macOS安装包](http://mblockapp.oss-cn-hongkong.aliyuncs.com/mblock5/darwin/mLink.pkg)

2\. 请按照提示，正确安装mLink

<img src="images/yb5.jpg" width="600px;" style="padding:3px 0px 10px 0px;">

<img src="images/yb6.jpg" width="600px;" style="padding:3px 0px 10px 0px;">

3\. 安装完毕后，双击启动台上的 mLink 的图标启动

<img src="images/en-7.png" width="100px;" style="padding:3px 0px 10px 0px;">

成功启动 mLink 的标志是，mLink 会出现在桌面右下角的任务栏里（如果不知道任务栏的位置，可参考[常见问题](#6)）。

<small>注：每次学习前，都需要启动mLink。</small>

<img src="images/yb7.png" width="600px;" style="padding:3px 0px 10px 0px;">

有的用户在安装过程中，会遇到以下问题。该种情况下，请选择 “保留”。

<img src="images/mac.png" style="padding:3px 0px 10px 0px;">

4\. 成功啦，进入第三步的操作

### 第三步：开启学习

使用Chrome浏览器访问 <a href='https://hezi.makeblock.com/#/' target='_blank'>hezi.makeblock.com</a>，开启学习

---

## 其余问题

- [一、谷歌浏览器安装指南](#1)
- [二、什么是mLink插件？](#2)
- [三、如何知道自己的电脑是 Windows 系统还是 MacOS 系统？](#3)
- [四、下载了mLink，没有找到文件的下载位置](#4)
- [五、如何知道自己的系统是哪个版本的？](#5)
- [六、电脑的任务栏在哪里？](#6)
- [七、PC用户无法连接硬件怎么办，比如显示「扫描串口中」？](#7)
- [八、无法上传程序怎么办？](#8)
- [九、iPad 无法蓝牙连接到设备怎么办？](#9)
- [十、为什么PC 用户无法发现设备?](#10)
- [十一、在线模式测试不了积木，上传模式上传失败](#11)
- [十二、语音识别正确率低](#12)
- [十三、为什么光环板背面的芯片有杂音？](#13)
- [十四、为什么光环板捏在手里会感觉温度逐渐升高？](#14)
- [十五、如何测试自己的mLink插件是否安装成功？（以光环板为例）](#15)

### 一、谷歌浏览器安装指南{#1}

请点击以下网址 [https://www.google.cn/chrome/](https://www.google.cn/chrome/) 按照提示安装Chrome浏览器。

### 二、什么是mLink插件？{#2}

mLink 插件是一个驱动小体积插件，能帮助你成功连接 童心制物编程盒 和 慧编程软件。

### 三、如何知道自己的电脑是 Windows 系统还是 MacOS 系统？{#3}

如果你无法判断自己的电脑是 Windows 还是 MacOS 系统，可查看电脑桌面的左上方或者左下角。

如果显示图标如下图所示，则是 MacOS 系统

<img src="images/mac-0.png" style="padding:3px 0px 10px 0px;" width="400px;">

如果显示图标如下图所示，则是 Windows 系统

<img src="images/windows.png" style="padding:3px 0px 10px 0px;" width="400px;">

### 四、下载了mLink，没有找到文件的下载位置{#4}

点击 Chrome 浏览器右上角的 <span style="border-radius:5px;"><img src="images/icon-chrome.png" height="24px;"></span> 图标，选择 “下载内容”。

<img src="images/download-1.png" style="padding:3px 0px 10px 0px;" width="400px;">

在 “下载内容” 页面即可找到 mLink 安装包。

<img src="images/download-2.png" style="padding:3px 0px 10px 0px;" width="800px;">

### 五、如何知道自己的系统是哪个版本的？{#5}

**Windows 系统**：找到【计算机】图标，右键点击【属性】，即可查看自己的系统版本

<img src="images/windows-1.png" style="padding:3px 0px 10px 0px;" width="100px;">

<img src="images/windows-2.png" style="padding:3px 0px 10px 0px;" width="600px;">

**MacOS 系统**：点击左上方苹果图标，右键按【关于本机】，即可查看系统版本

<img src="images/mac-00.png" style="padding:3px 0px 10px 0px;" width="400px;">

### 六、电脑的任务栏在哪里？{#6}

Window电脑的任务栏在桌面的右下方

<img src="images/install-8.png" style="padding:3px 0px 10px 0px;" width="400px;">

Mac电脑的任务栏在桌面的右上方

<img src="images/pic-5.png" style="padding:3px 0px 10px 0px;" width="600px;">

### 七、PC用户无法连接硬件怎么办，比如显示「扫描串口中」？{#7}

**Winodws 用户：**

驱动安装过程受到了安全软件的阻止，没有装成功，先卸载慧编程，然后关掉电脑上防护软件后（360卫士和电脑管家等工具），重新安装慧编程。

**MacOS 用户：**

<img src="images/mac-1.png" style="padding:3px 0px 10px 0px;">

打开 “系统偏好设置” — “安全性与隐私”，此时可以看到“来自开发者 jiangsu Qinheng Co. Ltd的系统软件已被阻止载入”提示,点击后面的“允许”按钮即可。如下图所示：

<img src="images/mac-2.png" style="padding:3px 0px 10px 0px;">

然后重启电脑，光环板就正常扫描连接。

<small>注意：该界面下边的“锁”，需要是在锁定情况下，而且需要是要选中第二个选项，才会出现“允许”的提示。如果当前是“任何来源”的选项，需要修改成第二个选项，“App Store 和被认可的开发者”，并且修改下面的锁按钮为锁定状态。</small>

#### 已经按照上述方法还是找不到串口？

1\. 安装驱动

<a href="images/CH34x_Install_V1.3.pkg.zip" download>CH34x_Install_V1.3.pkg.zip</a>

2\. 按照下列方式重启

[http://www.mblock.cc/zh-home/docs/zh-run-makeblock-ch340-ch341-on-mac-os-sierra/](http://www.mblock.cc/zh-home/docs/zh-run-makeblock-ch340-ch341-on-mac-os-sierra/)

### 八、无法上传程序怎么办？{#8}

1. 确保是连接状态（电脑用数据线、iPad 用蓝牙）
2. 确保在电脑上选择了合适的串口
    - 尽量使用原装数据线，部分质量差数据线无法上传程序
    - 不要使用一些手机的快充头为光环板供电，这可能导致无法正常工作

### 九、iPad 无法蓝牙连接到设备怎么办？{#9}

蓝牙连接可能受芯片位置和周围环境的影响，将 iPad 打开连接状态之后，记得尽量让 iPad 接近光环板。

### 十、为什么PC 用户无法发现设备?{#10}

光环板用 USB 线连接⾄至电脑后，打开慧编程点击 「**连接**」 按钮，显示 「**未找到可连接设备**」，如下图所示:

<img src="images/no-device-1.png" width="800px;" style="padding:5px 5px 20px 5px;">
 
A：出现这个现象，很⼤可能是以下两种原因造成的，下⾯针对这两种情况，给出对应的解决方法。

**1\) USB 数据线导致的问题**

市面上存在一类仅具备电力传输功能的数据线，使⽤该类数据线连接光环板和电脑，会导致电脑端无法正常检测到光环板。

所以手边有多余的数据线的话，可以多试几根线看看。建议用安卓手机自带的 MicroUSB 线（如果有的话），或者[点击购买](https://detail.tmall.com/item.htm?id=565555931655&skuId=3703559825974) Makeblock 官⽅方出品的 「100cm USB 数据线」。如下图所示：

<img src="images/no-device-2.png" width="400px;" style="padding:5px 5px 20px 5px;">


**2\) 驱动⽂件未正确安装**

由于电脑上运行了防护软件（系统自带或者第三方），导致慧编程在安装时，驱动文件未能正确安装，所以会出现电脑检测不到光环板的情况。

可以先将光环板从电脑端拔下，然后根据电脑操作系统，下载光环板所需要的驱动文件: 
- Windows 驱动⽂文件下载：[http://www.wch.cn/download/CH341SER_EXE.html](http://www.wch.cn/download/CH341SER_EXE.html)
- macOS 驱动⽂文件下载：[http://www.wch.cn/download/CH341SER_MAC_ZIP.html](http://www.wch.cn/download/CH341SER_MAC_ZIP.html)

手动安装驱动文件后，重新用 USB 线将光环板连接⾄电脑。再次点击慧编程中的「**连接**」 按钮，看是否有 COM 口出现（如下图所示）。

<img src="images/no-device-3.png" width="600px;" style="padding:5px 5px 20px 5px;">

### 十一、在线模式测试不了积木，上传模式上传失败{#11}

很有可能是电脑的USB接口松动。换个USB口插。再不行，换台电脑使用。	

### 十二、语音识别正确率低{#12}

- 保证周边安静的环境，换个网络试试。
- 不要使用需手机号等二次验证的网络。
- 尽量不要使用WiFi 名称为中文的网络。

### 十三、为什么光环板背面的芯片有杂音？{#13}

芯片工作的正常现象。

### 十四、为什么光环板捏在手里会感觉温度逐渐升高？{#14}

芯片计算会发热，但温度一般不会有危险。

### 十五、如何测试自己的mLink插件是否安装成功？（以光环板为例）{#15}

1\. 打开Chrome浏览器，登录网址：[ide.makeblock.com](https://ide.makeblock.com/#/)

2\. 使用 Micro-USB 数据线将光环板连接到电脑的 USB 口。

<img src="images/get-started-3.png" style="width:300px" style="padding:3px 0px 10px 0px;">

3\. 选中“设备”，点击“+”添加设备。

<img src="images/halo-1.png" style="padding:3px 0px 10px 0px;">

4\. 在弹出的设备库页面，选中“光环板”，点击“确定”。

<img src="images/halo-2.png" style="padding:3px 0px 10px 0px;">

5\. 选中“光环板”，点击“连接”。

<img src="images/halo-3.png" style="padding:3px 0px 10px 0px;">

6\. 连接设备窗口会弹出，慧编程会自动检测光环板的串口，请点击“连接”。

<small>注：“COM3”为串口序号，不同系统及电脑会有所不同。直接点击“连接”即可。</small>

<img src="images/halo-4.png" style="padding:3px 0px 10px 0px;">

---

## 如有更多问题

可联系售后/所在班级的班主任了解更多。

---